## Extended SRL for RTE

This is the reimplementation of [Chang et al.'s 2010](http://cogcomp.cs.illinois.edu/page/publication_view/214)
__LCRL__ system as well as an attempt to build [Sammon's et al.'s 2009](http://cogcomp.cs.illinois.edu/page/publication_view/192)
__RATER__ system. To replicate the RTE5 results you will need the [dev](http://cogcomp.cs.illinois.edu/page/corpora_view/154)
and [test](http://cogcomp.cs.illinois.edu/page/corpora_view/155) datasets.

To replicate the __LCLR__ results, run:

    mvn compile dependency:copy-dependencies
    java -cp target/classes:target/dependency/* edu.illinois.cs.cogcomp.esrl.Main runLatentRTE 5.0 30 NoMergeExtend

For __RATER__:

    mvn compile dependency:copy-dependencies
    java -cp target/classes:target/dependency/* edu.illinois.cs.cogcomp.esrl.Main trainRater

