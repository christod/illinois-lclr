package edu.illinois.cs.cogcomp.esrl.latent.features;

/**
 * Created to replace the use of liblinear's FeatureNode
 */
public class Feature {
    private final int index;
    private final double value;

    public Feature(int index, double value) {
        if(index < 1) {
            throw new IllegalArgumentException("index must be >= 1");
        } else {
            this.index = index;
            this.value = value;
        }
    }

    public int getIndex() {
        return index;
    }

    public double getValue() {
        return value;
    }
}
