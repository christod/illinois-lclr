package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.cachingcurator.CachingAnnotator;
import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.edison.annotators.HeadFinderDependencyViewGenerator;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;
import org.apache.thrift.TException;

/**
 * A wrapper of {@link HeadFinderDependencyViewGenerator} to be used with {@link CachingCurator}
 */
public class HeadFinderViewGenerator implements CachingAnnotator {
    public static final String VIEW_NAME = ViewNames.DEPENDENCY + ":" + ViewNames.PARSE_CHARNIAK;

    @Override
    public View getView(TextAnnotation ta, CachingCurator cachingCurator) throws TException,
            AnnotationFailedException, ServiceUnavailableException {
        cachingCurator.requestView(ta, ViewNames.PARSE_CHARNIAK);

        if (!ta.hasView(ViewNames.PARSE_CHARNIAK))
            throw new AnnotationFailedException("Cannot provide " + VIEW_NAME + " without " + ViewNames.PARSE_CHARNIAK);

        HeadFinderDependencyViewGenerator dependencyViewGenerator
                = new HeadFinderDependencyViewGenerator(ViewNames.PARSE_CHARNIAK);

        return dependencyViewGenerator.getView(ta);
    }

    @Override
    public String[] getRequiredViews() {
        return new String[]{ViewNames.PARSE_CHARNIAK};
    }

    @Override
    public String getViewName() {
        return VIEW_NAME;
    }

    @Override
    public View getView(TextAnnotation ta) {
        return null;
    }
}
