/**
 *
 */
package edu.illinois.cs.cogcomp.esrl.latent.views;

import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;

import java.io.Serializable;
import java.util.List;

/**
 * This interface represents the DAG that captures the semantics of some text.
 * The nodes of this DAG are {@link Constituent}s and the directed edges are
 * {@link Relation}s. Additionally, there is an assumption that this DAG has a
 * single unique root constituent.
 *
 * @author Vivek Srikumar
 *         <p/>
 *         Aug 24, 2009
 */
public interface ISemanticDAG extends Serializable {
    /**
     * Get a list of the nodes of this DAG
     *
     * @return A list of constituents, each of which represents a node
     */
    List<Constituent> getNodes();

    /**
     * Get a list of edges of this DAG.
     *
     * @return A list of relations, each of which represents an edge.
     */
    List<Relation> getEdges();

    /**
     * Gets the root element of this DAG.
     *
     * @return The root element
     */
    Constituent getRoot();
}
