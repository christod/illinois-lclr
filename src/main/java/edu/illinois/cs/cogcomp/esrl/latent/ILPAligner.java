package edu.illinois.cs.cogcomp.esrl.latent;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.esrl.data.TEPair;
import edu.illinois.cs.cogcomp.esrl.latent.features.Feature;
import edu.illinois.cs.cogcomp.esrl.latent.features.TEFeatureGenerator;
import edu.illinois.cs.cogcomp.esrl.data.DAGViewGenerator;
import edu.illinois.cs.cogcomp.esrl.latent.views.ISemanticDAG;
import edu.illinois.cs.cogcomp.lbjava.infer.GurobiHook;
import edu.illinois.cs.cogcomp.lbjava.infer.ILPSolver;

import java.io.IOException;
import java.util.*;

public class ILPAligner {
    final private TEFeatureGenerator featureGenerator;
    private Hashtable<Integer, Constituent[]> constituentPairs = new Hashtable<>();
    private Hashtable<String, Integer> constituentPairsToIndices = new Hashtable<>();
    private Hashtable<Integer, Relation[]> relationPairs = new Hashtable<>();
    private Hashtable<Integer, Double> variableWeights = new Hashtable<>();
    private ILPSolver xmp = new GurobiHook();
    private LexManager lexManager;
    private WeightVector weightVector;


    public ILPAligner(TEFeatureGenerator fea, LexManager lm, WeightVector wv) {
        featureGenerator = fea;
        lexManager = lm;
        weightVector = wv;
    }

    private TEAlignment align(ISemanticDAG textDAG, ISemanticDAG hypDAG) {
        constituentPairs.clear();
        constituentPairsToIndices.clear();
        relationPairs.clear();
        variableWeights.clear();

        xmp.reset();

        List<Constituent> textNodes = textDAG.getNodes();
        List<Constituent> hypNodes = hypDAG.getNodes();

        List<Relation> textRelations = textDAG.getEdges();
        List<Relation> hypRelations = hypDAG.getEdges();

        Hashtable<Constituent, Vector<Constituent>> textMappingConstraints = new Hashtable<>();

        for (Constituent hypothesisConstituent : hypNodes) {
            if (hypothesisConstituent.getLabel().equals("TOP")) continue;

            Vector<Integer> hypConstituents = new Vector<>();
            for (Constituent textConstituent : textNodes) {
                if (textConstituent.getLabel().equals("TOP")) continue;

                if (!textMappingConstraints.containsKey(textConstituent)) {
                    textMappingConstraints.put(textConstituent, new Vector<Constituent>());
                }
                textMappingConstraints.get(textConstituent).add(hypothesisConstituent);

                Constituent[] pair = new Constituent[2];
                pair[0] = hypothesisConstituent;
                pair[1] = textConstituent;

                double nodePairScore = weightVector.dotProduct(lexManager.convertRawFeaMap2LRFeatures(
                        featureGenerator.getNodeAlignmentFeatures(textConstituent, hypothesisConstituent)));

                int varIndex = xmp.addBooleanVariable(nodePairScore);
                constituentPairs.put(varIndex, pair);
                constituentPairsToIndices.put(pair[0].getStartSpan() + "&" + pair[1].getStartSpan(), varIndex);
                hypConstituents.add(varIndex);
                variableWeights.put(varIndex, nodePairScore);
            }

            int[] cIndices = new int[hypConstituents.size()];
            for (int i = 0; i < hypConstituents.size(); i++)
                cIndices[i] = hypConstituents.get(i);

            xmp.addEqualityConstraint(cIndices, getCoefficientArray(cIndices.length), 1);
        }

        for (Constituent textConstituent : textMappingConstraints.keySet()) {
            Vector<Constituent> hypConstituents = textMappingConstraints.get(textConstituent);
            int[] cIndices = new int[hypConstituents.size()];
            for (int i = 0; i < hypConstituents.size(); i++) {
                cIndices[i] = constituentPairsToIndices
                        .get(hypConstituents.get(i).getStartSpan() + "&" + textConstituent.getStartSpan());
            }
            xmp.addLessThanConstraint(cIndices, getCoefficientArray(cIndices.length), 1);
        }

        for (Relation hypothesisRelation : hypRelations) {
            if (hypothesisRelation.getSource().getLabel().equals("TOP")) continue;

            for (Relation textRelation : textRelations) {
                if (textRelation.getSource().getLabel().equals("TOP")) {
                    continue;
                }

                Relation[] pair = new Relation[2];
                pair[0] = hypothesisRelation;
                pair[1] = textRelation;

                double edgePairScore = weightVector.dotProduct(lexManager.convertRawFeaMap2LRFeatures(
                        featureGenerator.getEdgeAlignmentFeatures(textRelation, hypothesisRelation)));
                int varIndex = xmp.addBooleanVariable(edgePairScore);

                relationPairs.put(varIndex, pair);
                variableWeights.put(varIndex, edgePairScore);

                int sourceConstVarIndex = constituentPairsToIndices
                        .get(hypothesisRelation.getSource().getStartSpan() + "&" + textRelation.getSource().getStartSpan());

                int targetConstVarIndex = constituentPairsToIndices
                        .get(hypothesisRelation.getTarget().getStartSpan() + "&" + textRelation.getTarget().getStartSpan());

                // add sourceConstVarIndex \and targetConstVarIndex \implies inferenceVariablesIndex
                int[] nodesToRelationConsistencyConstraint = new int[3];
                double[] coefs = new double[3];
                nodesToRelationConsistencyConstraint[0] = sourceConstVarIndex;
                coefs[0] = -1;
                nodesToRelationConsistencyConstraint[1] = targetConstVarIndex;
                coefs[1] = -1;
                nodesToRelationConsistencyConstraint[2] = varIndex;
                coefs[2] = 1;
                xmp.addGreaterThanConstraint(nodesToRelationConsistencyConstraint, coefs, -1);

                // add inferenceVariablesIndex \implies sourceConstVarIndex \and targetConstVarIndex
                int[] relationToNodesConsistencyConstraint = new int[2];
                coefs = new double[2];
                relationToNodesConsistencyConstraint[0] = sourceConstVarIndex;
                coefs[0] = 1;
                relationToNodesConsistencyConstraint[1] = varIndex;
                coefs[1] = -1;
                xmp.addGreaterThanConstraint(relationToNodesConsistencyConstraint, coefs, 0);

                relationToNodesConsistencyConstraint = new int[2];
                coefs = new double[2];
                relationToNodesConsistencyConstraint[0] = targetConstVarIndex;
                coefs[0] = 1;
                relationToNodesConsistencyConstraint[1] = varIndex;
                coefs[1] = -1;
                xmp.addGreaterThanConstraint(relationToNodesConsistencyConstraint, coefs, 0);
            }
        }

        // Solving the ILP problem
        xmp.setMaximize(true);
        try {
            boolean solved = xmp.solve();
            if (solved) {
                TEAlignment alignment = new TEAlignment(textDAG, hypDAG);

                double sum = 0;
                for (int variable : variableWeights.keySet()) {
                    if (xmp.getBooleanValue(variable)) {
                        sum += variableWeights.get(variable);
                        Constituent[] res = constituentPairs.get(variable);
                        if (res != null) {
                            alignment.addNodeAlignmentFromHypothesisToText(res[0], res[1]);
                        } else {
                            Relation[] relationRes = relationPairs.get(variable);
                            alignment.addEdgeAlignmentFromHypothesisToText(relationRes[0], relationRes[1]);
                        }
                    }
                }
                alignment.setScore(sum);
                return alignment;
            }
            else {
                System.err.println("Unsat");
                return null;
            }
        }
        catch (Exception e) {
            System.err.println("Solver exception:");
            e.printStackTrace();
            return null;
        }
    }

    private double[] getCoefficientArray(int size) {
        double[] result = new double[size];
        Arrays.fill(result, 1);
        return result;
    }

    /**
     * Performs alignment and returns the feature vector of the best aligned nodes/edges.
     * Used during training/inference.
     * @param pair The TEPair to extract features from
     * @return The feature vector of the best alignment
     */
    public Feature[] getBestFeatures(TEPair pair, String split, boolean finalAlignment) {
        ISemanticDAG tSemanticDag = (ISemanticDAG) pair.getText().getView(DAGViewGenerator.VIEW_NAME);
        ISemanticDAG hSemanticDag = (ISemanticDAG) pair.getHypothesis().getView(DAGViewGenerator.VIEW_NAME);
        TEAlignment alignment;

        Map<String, Double> featureMap = new HashMap<>();

        int n_round = 5;
        if (pair.getLabel().equalsIgnoreCase(TEDataHandler.ENTAILMENT)) {
            for (int t = 0; t < n_round; t++) {
                alignment = align(tSemanticDag, hSemanticDag);
                Map<String, Double> single_fea_map = new HashMap<>();
                if (alignment != null) {
                    single_fea_map = alignment.getMapFeatures(featureGenerator);
                    if (finalAlignment && t == (n_round - 1)) {
                        try {
                            LineIO.append(split+"_align.out", pair.getId() + "\t" + alignment.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    System.out.println("No solution found");
                    if (finalAlignment && t == (n_round - 1)) {
                        try {
                            LineIO.append(split+"_align.out", pair.getId() + "\tnull");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                single_fea_map.put("*global-bias*", 1.0);
                for (String s : single_fea_map.keySet()) {
                    if (!featureMap.containsKey(s))
                        featureMap.put(s, single_fea_map.get(s));
                    featureMap.put(s, featureMap.get(s) + single_fea_map.get(s));
                }
            }

            for (String s : featureMap.keySet()) {
                featureMap.put(s, featureMap.get(s) / n_round);
            }
        }
        else {
            alignment = align(tSemanticDag, hSemanticDag);
            if (alignment != null) {
                featureMap = alignment.getMapFeatures(featureGenerator);
                if (finalAlignment) {
                    try {
                        LineIO.append(split+"_align.out", pair.getId() + "\t" + alignment.toString());
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                System.out.println("No solution found");
                if (finalAlignment) {
                    try {
                        LineIO.append(split+"_align.out", pair.getId() + "\tnull");
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            featureMap.put("*global-bias*", 1.0);
        }
        return lexManager.convertRawFeaMap2LRFeatures(featureMap);
    }
}
