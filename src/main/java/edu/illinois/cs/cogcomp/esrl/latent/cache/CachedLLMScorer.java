package edu.illinois.cs.cogcomp.esrl.latent.cache;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.esrl.Annotator;
import edu.illinois.cs.cogcomp.esrl.Properties;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class CachedLLMScorer implements ICachedScorer {
    private Map<String, Double> cache = null;
    private String cache_file_name = "LLM_CACHE";
    private static final String sep = "$$$$$";
    private Annotator annotator;

    public CachedLLMScorer() {
        try {
            this.annotator = Annotator.getInstance();
            cache_file_name = Properties.llmCacheFile;
            String cacheDir = ScoreManager.cacheDir;

            if (IOUtils.exists(cacheDir + "/" + cache_file_name)) {
                System.out.println("LOAD LLMcache from " + cacheDir + "/" + cache_file_name);
                ObjectInputStream ois = new ObjectInputStream(
                        new BufferedInputStream(new FileInputStream(cacheDir + "/" + cache_file_name)));
                cache = (Map<String, Double>) ois.readObject();
                ois.close();
            }
            else cache = new HashMap<>();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public double getScore(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        if (s1.length() > s2.length()) {
            String tmp = s2;
            s2 = s1;
            s1= tmp;
        }

        boolean num1 = true;
        boolean num2 = true;
        double d1 = 0, d2 = 0;
        try {
            d1 = Double.parseDouble(s1);
        } catch (NumberFormatException e) {
            num1 = false;
        }

        try {
            d2 = Double.parseDouble(s2);
        } catch (NumberFormatException e) {
            num2 = false;
        }

        if (num1 != num2)
            return 0;
        else if (num1 && (d1 != d2))
            return 0;
        else if (cache.containsKey(s1 + sep + s2))
            return cache.get(s1 + sep + s2);
        else {
            double dLocal = annotator.getLLM(s1, s2);
            cache.put(s1 + sep + s2, dLocal);
            return dLocal;
        }
    }

    public void save() throws IOException {
        System.out.println("Save WSIM cache...");
        System.out.flush();
        ObjectOutputStream oos = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(ScoreManager.cacheDir + "/" + cache_file_name)));
        oos.writeObject(cache);
        oos.close();
        System.out.println("Done...");
        System.out.flush();
    }
}
