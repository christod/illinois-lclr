package edu.illinois.cs.cogcomp.esrl.rater;

import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;

/**
 * An interface to LBJava's trainer to avoid training during compilation
 */
public class RaterTrainer {
    //private RATERClassifier classifier;
    private static final String DIR = "src/main/resources/";
    private static final String modelFile = DIR + "TEClassifier.lc";
    private static final String lexiconFile = DIR + "TEClassifier.lex";
    private static final String trainExampleFilePath = DIR + "TEClassifier.ex";
    private static final String testExampleFilePath = DIR + "TEClassifier.test.ex";
    private static final int progressOutputEvery = 50;
    String trainData, testData;

    public RaterTrainer(String trainData, String testData) {
        //classifier = new RATERClassifier(modelFile, lexiconFile);
        //classifier.forget();
        this.trainData = trainData;
        this.testData = testData;
    }

    public void train(int iterNum) throws Exception {
        //BatchTrainer batchTrainer = new BatchTrainer(classifier, new ACL10Reader(trainData), progressOutputEvery);
        //classifier.setLexicon(batchTrainer.preExtract(trainExampleFilePath));
        //BatchTrainer batchTester = new BatchTrainer(classifier, new ACL10Reader(testData), progressOutputEvery, "test set: ");
        //classifier.setLexicon(batchTester.preExtract(testExampleFilePath));

        //batchTrainer.train(iterNum);
        //classifier.save();

        System.out.println("Testing TE classifer: ");
        //overall precision, overall recall, F1, and accuracy.
        TestDiscrete simpleTest = new TestDiscrete();
        //Parser testParser = batchTester.getParser();
        //TestDiscrete.testDiscrete(simpleTest, classifier, null, testParser, true, 0);
    }
}
