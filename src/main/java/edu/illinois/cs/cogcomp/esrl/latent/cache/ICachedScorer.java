package edu.illinois.cs.cogcomp.esrl.latent.cache;

import java.io.IOException;

/**
 * The interface for all the (cached) scorers
 */
public interface ICachedScorer {
    double getScore(String s1, String s2);
    void save() throws IOException;
}
