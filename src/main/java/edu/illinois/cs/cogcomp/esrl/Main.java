package edu.illinois.cs.cogcomp.esrl;

import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.core.experiments.ClassificationTester;
import edu.illinois.cs.cogcomp.core.experiments.NotificationSender;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandDescription;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandIgnore;
import edu.illinois.cs.cogcomp.core.utilities.commands.InteractiveShell;
import edu.illinois.cs.cogcomp.esrl.data.TEPair;
import edu.illinois.cs.cogcomp.esrl.latent.ILPAligner;
import edu.illinois.cs.cogcomp.esrl.latent.LexManager;
import edu.illinois.cs.cogcomp.esrl.latent.TEDataHandler;
import edu.illinois.cs.cogcomp.esrl.latent.WeightVector;
import edu.illinois.cs.cogcomp.esrl.latent.cache.ScoreManager;
import edu.illinois.cs.cogcomp.esrl.latent.features.TEFeatureGenerator;
import edu.illinois.cs.cogcomp.esrl.rater.RaterTrainer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static NotificationSender notifier;

    @CommandIgnore
    public static void main(String args[]) throws Exception {
        Properties.readProperties("config/latent.properties");
        notifier = new NotificationSender("/home/christod/.notify/api-key.txt", "ESRL/RTE", "Finished!");
        InteractiveShell<Main> tester = new InteractiveShell<>(Main.class);
        if (args.length == 0)
            tester.showDocumentation();
        else {
            long start_time = System.currentTimeMillis();
            tester.runCommand(args);
            System.out.println("This experiment took "
                    + (System.currentTimeMillis() - start_time) / 60000.0
                    + " minutes");
        }
    }

    @CommandDescription(description = "Trains the RATER system of Sammons et al. (2010)", usage = "trainRater")
    public static void trainRater() throws Exception {
        String data = "rte5";
        String[] file_pair = getTEFilesPair(data);

        RaterTrainer raterTrainer = new RaterTrainer(file_pair[0], file_pair[1]);
        // There is no need to open the cache since it will be opened automatically on first use
        // The cache's location is set up in Annotator.java
        CachingCurator.closeCache();

        raterTrainer.train(5);
    }

    @CommandDescription(description = "Trains the Latent SVM system of Chang et al. (2010)",
            usage = "runLatentRTE SVM-C <double> num-iters <int>")
    public static void runLatentRTE(String cStr, String outIterStr) throws Exception {
        String data = "rte5";
        String[] filePair = getTEFilesPair(data);

        LexManager lm = new LexManager();
        WeightVector wv = new WeightVector();
        String featureName = "node-pair-with-coref-wnsim";
        lm.previewFeature(featureName);
        wv.setWElement(lm.getFeatureID(featureName), 1.0);
        featureName = "edge-conjunction-score";
        lm.previewFeature(featureName);
        wv.setWElement(lm.getFeatureID(featureName), 1.0);

        double C = Double.parseDouble(cStr);
        int outIter = Integer.parseInt(outIterStr);

        TEFeatureGenerator fea = new TEFeatureGenerator();

        ILPAligner featureFinder = new ILPAligner(fea, lm, wv);

        List<TEPair> trainData = TEDataHandler.readRaterTEData(filePair[0]);
        List<TEPair> testData = TEDataHandler.readRaterTEData(filePair[1]);
        // There is no need to open the cache since it will be opened automatically on first use
        // The cache's location is set up in Annotator.java
        CachingCurator.closeCache();

        List<Integer> trainLabels = new ArrayList<>();
        for (TEPair tep : trainData) {
            if (tep.getLabel().equalsIgnoreCase(TEDataHandler.ENTAILMENT))
                trainLabels.add(1);
            else
                trainLabels.add(-1);
        }

        wv.updateWeightVectorWithFullCDLatentSVM(featureFinder, trainLabels, trainData, outIter, C);

        double[] trainScore = wv.predictScoreByFindingFeatures(featureFinder, "train", trainData);
        System.out.println("Train score: ");
        double trainAcc = evaluate(testData, trainScore);
        notifier.notify("Accuracy: " + StringUtils.getFormattedTwoDecimal(trainAcc));

        double[] testScore = wv.predictScoreByFindingFeatures(featureFinder, "test", testData);
        System.out.println("Test score: ");
        double testAcc = evaluate(testData, testScore);
        notifier.notify("Accuracy: " + StringUtils.getFormattedTwoDecimal(testAcc));

        // cache the similarity scores
        ScoreManager.save();
    }

    private static String[] getTEFilesPair(String data_name) throws Exception {
        String[] pair = new String[2];
        if (data_name.equalsIgnoreCase("RTE5")) {
            //pair[0] = "data/acl10/data/RTE5_MainTask_DevSet_clean.xml";
            //pair[1] = "data/acl10/data/RTE5_MainTask_TestSet_Gold_clean.xml";
            pair[0] = "data/acl10/data/snli_1.0_train.xml";
            pair[1] = "data/acl10/data/snli_1.0_dev.xml";
        }
        else
            throw new Exception("undefined RTE dataset!");

        return pair;
    }

    private static double evaluate(List<TEPair> tePairList, double[] score) throws IOException {
        assert score.length == tePairList.size();
        double[] gold = new double[tePairList.size()];
        double[] prediction = new double[score.length];

        ClassificationTester tester = new ClassificationTester();
        List<String> outputLines = new ArrayList<>();
        double tp = 0.0;
        double fp = 0.0;
        double fn = 0.0;
        double tn = 0.0;

        for (int i = 0; i < score.length; i++) {
            if (score[i] > 0)
                prediction[i] = 1;
            else
                prediction[i] = -1;

            TEPair tePair = tePairList.get(i);
            if (tePair.getLabel().equalsIgnoreCase(TEDataHandler.ENTAILMENT))
                gold[i] = 1;
            else
                gold[i] = -1;

            if (prediction[i] == 1 && gold[i] == 1)
                tp += 1;
            else if (prediction[i] == -1 && gold[i] == 1)
                fn += 1;
            else if (prediction[i] == 1 && gold[i] == -1)
                fp += 1;
            else
                tn += 1;


            String goldStr = tePair.getLabel();
            String predictionStr = (score[i] > 0) ? TEDataHandler.ENTAILMENT : TEDataHandler.CONTRADICTION;
            tester.record(goldStr, predictionStr);

            outputLines.add(tePair.getId() + "\t" + goldStr + "\t" + predictionStr);
        }
        LineIO.write("results.out", outputLines);

        double acc = (tp + tn) / (tp + tn + fp + fn) * 100;
        System.out.println("There are " + (tp + fn) + " positive examples. Among them, " + tp + " are correct.");
        System.out.println("There are " + (tn + fp) + " negative examples. Among them, " + tn + " are correct.");
        System.out.println("ACC is : " + StringUtils.getFormattedTwoDecimal(acc));
        System.out.println();
        System.out.println(tester.getPerformanceTable().toOrgTable());

        return acc;
    }

    @CommandDescription(description = "Evaluates an output file",
            usage = "evaluate <file>")
    public static void evaluate(String file) throws FileNotFoundException {
        ClassificationTester tester = new ClassificationTester();
        List<String> lines = LineIO.readLines(file);
        double tp = 0.0;
        double fp = 0.0;
        double fn = 0.0;
        double tn = 0.0;
        // Format: id    gold    prediction
        for (String line : lines) {
            String gold = line.split("\t")[1];
            if (gold.equals(TEDataHandler.UNKNOWN))
                gold = TEDataHandler.CONTRADICTION;
            String prediction = line.split("\t")[2];
            int goldScore;
            if (gold.equalsIgnoreCase(TEDataHandler.ENTAILMENT))
                goldScore = 1;
            else
                goldScore = -1;
            int predScore;
            if (prediction.equalsIgnoreCase(TEDataHandler.ENTAILMENT))
                predScore = 1;
            else
                predScore = -1;
            if (predScore == 1 && goldScore == 1)
                tp += 1;
            else if (predScore == -1 && goldScore == 1)
                fn += 1;
            else if (predScore == 1 && goldScore == -1)
                fp += 1;
            else
                tn += 1;
            tester.record(gold, prediction);
        }
        double acc = (tp + tn) / (tp + tn + fp + fn) * 100;
        System.out.println("There are " + (tp + fn) + " positive examples. Among them, " + tp + " are correct.");
        System.out.println("There are " + (tn + fp) + " negative examples. Among them, " + tn + " are correct.");
        System.out.println("ACC is : " + StringUtils.getFormattedTwoDecimal(acc));
        System.out.println();
        System.out.println(tester.getPerformanceTable().toOrgTable());
    }
}
