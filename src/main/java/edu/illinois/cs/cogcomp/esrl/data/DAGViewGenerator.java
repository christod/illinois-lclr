package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.cachingcurator.CachingAnnotator;
import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.comma.CommaLabeler;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.esrl.latent.views.SemanticTree;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;
import org.apache.thrift.TException;

import java.util.Arrays;

/**
 * A wrapper for {@link SemanticTree} to be used with {@link CachingCurator}
 *
 * @author Christos Christodoulopoulos
 */
public class DAGViewGenerator implements CachingAnnotator {
    public static final String VIEW_NAME = "SemanticDagView";

    @Override
    public View getView(TextAnnotation ta, CachingCurator cachingCurator) throws TException, AnnotationFailedException,
            ServiceUnavailableException {
        // Make sure we have the required views
        cachingCurator.requestView(ta, ViewNames.LEMMA);
        cachingCurator.requestView(ta, ViewNames.POS);
        cachingCurator.requestView(ta, ViewNames.PARSE_CHARNIAK);
        cachingCurator.requestView(ta, ViewNames.NER);
        cachingCurator.requestView(ta, ViewNames.COREF);
        cachingCurator.requestView(ta, ViewNames.SRL_VERB);
        cachingCurator.requestView(ta, ViewNames.SRL_NOM);
        cachingCurator.requestView(ta, ViewNames.SRL_PREP);
        cachingCurator.requestView(ta, CommaLabeler.VIEW_NAME);

        if (!hasRequiredAnnotations(ta))
            throw new AnnotationFailedException("Cannot provide " + VIEW_NAME + " without required views: "
                    + Arrays.toString(getRequiredViews()));

        return new SemanticTree(ta);
    }

    public static boolean hasRequiredAnnotations(TextAnnotation ta) {
        DAGViewGenerator generator = new DAGViewGenerator();
        for (String view : generator.getRequiredViews()) {
            if (!ta.hasView(view)) return false;
        }
        return true;
    }

    @Override
    public String[] getRequiredViews() {
        return new String[]{
                ViewNames.LEMMA,
                ViewNames.POS,
                ViewNames.PARSE_CHARNIAK,
                ViewNames.SRL_VERB,
                ViewNames.SRL_NOM,
                ViewNames.SRL_PREP,
                CommaLabeler.VIEW_NAME,
                ViewNames.NER,
                ViewNames.COREF
        };
    }

    @Override
    public String getViewName() {
        return VIEW_NAME;
    }

    @Override
    public View getView(TextAnnotation ta) {
        // Assume that we have the required annotations
        return new SemanticTree(ta);
    }
}
