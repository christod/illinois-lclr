package edu.illinois.cs.cogcomp.esrl.latent;

import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.esrl.latent.features.TEFeatureGenerator;
import edu.illinois.cs.cogcomp.esrl.latent.views.ISemanticDAG;

import java.util.*;

public class TEAlignment {
    private ISemanticDAG text;
    private ISemanticDAG hyp;
    private Map<Relation, Relation> edgeAlignmentFromHypothesisToText;
    private Map<Constituent, Constituent> nodeAlignmentFromHypothesisToText;
    private double score;

    /**
     * An alignment between the DAGs of the Text and Hypothesis. Created only once {@link ILPAligner} has found
     * the optimal alignment.
     * @param text The Text DAG
     * @param hypothesis The Hypothesis DAG
     */
    public TEAlignment(ISemanticDAG text, ISemanticDAG hypothesis) {
        this.text = text;
        this.hyp = hypothesis;
        this.score = 0;
        nodeAlignmentFromHypothesisToText = new HashMap<>();
        edgeAlignmentFromHypothesisToText = new HashMap<>();
    }

    private void addMapsInPlace(Map<String, Double> accumulator, Map<String, Double> toAdd) {
        for (String s : toAdd.keySet()) {
            if (!accumulator.containsKey(s))
                accumulator.put(s, toAdd.get(s));
            accumulator.put(s, accumulator.get(s) + toAdd.get(s));
        }
    }

    public void addNodeAlignmentFromHypothesisToText(Constituent hc, Constituent tc) {
        nodeAlignmentFromHypothesisToText.put(hc, tc);
    }

    public void addEdgeAlignmentFromHypothesisToText(Relation hr, Relation tr) {
        edgeAlignmentFromHypothesisToText.put(hr, tr);
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    /**
     * Generates the features from the aligned nodes/edges. Used during training/inference.
     * @param featureGenerator The feature generator
     * @return The HashMap containing the features of the alignment
     */
    public Map<String, Double> getMapFeatures(TEFeatureGenerator featureGenerator) {
        Map<String, Double> featureMap = new HashMap<>();

        for (Constituent hypothesisNode : nodeAlignmentFromHypothesisToText.keySet()) {
            Constituent textNode = nodeAlignmentFromHypothesisToText.get(hypothesisNode);

            Map<String, Double> nodeFeatures = featureGenerator.getNodeAlignmentFeatures(textNode, hypothesisNode);
            addMapsInPlace(featureMap, nodeFeatures);
        }

        for (Relation hypothesisEdge : edgeAlignmentFromHypothesisToText.keySet()) {
            Relation textEdge = edgeAlignmentFromHypothesisToText.get(hypothesisEdge);

            Map<String, Double> edgeFeatures = featureGenerator.getEdgeAlignmentFeatures(textEdge, hypothesisEdge);
            addMapsInPlace(featureMap, edgeFeatures);
        }

        double normalize_factor = hyp.getNodes().size() - 1; //remove the top node

        for (String key : featureMap.keySet()) {
            double value = featureMap.get(key) / normalize_factor;
            featureMap.put(key, value);
        }
        return featureMap;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("TEXT: ").append(((View) text).getTextAnnotation().getText()).append("\n");
        sb.append("HYP : ").append(((View) hyp).getTextAnnotation().getText()).append("\n");

        sb.append("TEXT NODES: ");
        List<Constituent> tns = text.getNodes();
        Collections.sort(tns, new Comparator<Constituent>() {

                    @Override
                    public int compare(Constituent o1, Constituent o2) {
                        if (o1.getStartSpan() < o2.getStartSpan())
                            return -1;
                        else if (o1.getStartSpan() > o2.getStartSpan())
                            return 1;
                        else
                            return 0;
                    }
                }
        );

        for (Constituent tc : tns) {
            if (tc.getLabel().equals("TOP")) continue;
            sb.append(" [ ").append(tc.getSurfaceString()).append(" ] ");
        }

        sb.append("\n");

        sb.append("HYP NODES: ");
        List<Constituent> hns = hyp.getNodes();
        Collections.sort(hns, new Comparator<Constituent>() {

                    @Override
                    public int compare(Constituent o1, Constituent o2) {
                        if (o1.getStartSpan() < o2.getStartSpan())
                            return -1;
                        else if (o1.getStartSpan() > o2.getStartSpan())
                            return 1;
                        else
                            return 0;
                    }
                }
        );

        for (Constituent hc : hns) {
            if (hc.getLabel().equals("TOP")) continue;
            sb.append(" [ ").append(hc.getSurfaceString()).append(" ] ");
        }

        sb.append("\n\n");

        sb.append("Node Alignment ====>\n");
        Set<Constituent> set = nodeAlignmentFromHypothesisToText.keySet();
        Constituent[] keySet = new Constituent[set.size()];
        int idx = 0;
        for (Constituent c : set) {
            keySet[idx] = c;
            idx++;
        }

        Arrays.sort(keySet, new Comparator<Constituent>() {
            @Override
            public int compare(Constituent arg0, Constituent arg1) {
                return arg0.toString().compareTo(arg1.toString());
            }
        });

        for (Constituent hc : keySet) {
            Constituent tc = nodeAlignmentFromHypothesisToText.get(hc);
            sb.append("\t");
            sb.append("(").append(hc.getSurfaceString()).append(")");
            sb.append(" ====> ");
            sb.append("(").append(tc.getSurfaceString()).append(" ").
                    append(tc.getStartSpan()).append("-").append(tc.getEndSpan()).append(")\t");
            sb.append("\n");
        }
        sb.append("\n");
        sb.append("Edge Alignment ====>\n");

        Set<Relation> rset = edgeAlignmentFromHypothesisToText.keySet();
        Relation[] rkeySet = new Relation[rset.size()];

        idx = 0;
        for (Relation r : rset) {
            rkeySet[idx] = r;
            idx++;
        }

        Arrays.sort(rkeySet, new Comparator<Relation>() {
            @Override
            public int compare(Relation arg0, Relation arg1) {
                return arg0.toString().compareTo(arg1.toString());
            }
        });


        for (Relation hr : rkeySet) {
            sb.append("\t");
            sb.append("(").append(hr.getSource().getSurfaceString()).append(" ----")
                    .append(hr.getRelationName()).append("----> ").append(hr.getTarget().getSurfaceString());
            sb.append(" ====> ");
            Relation tr = edgeAlignmentFromHypothesisToText.get(hr);
            sb.append("(").append(tr.getSource().getSurfaceString()).append(" ----")
                    .append(tr.getRelationName()).append("----> ").append(tr.getTarget().getSurfaceString());
            sb.append("\n");
        }
        sb.append("\n");
        sb.append("TOTAL Score ====>  \n");
        sb.append("\t");
        sb.append(score).append(" (unnormalized) \n");
        sb.append("\t");
        sb.append(score / (hyp.getNodes().size() - 1)).append(" (normalized) \n");
        return sb.toString();
    }
}
