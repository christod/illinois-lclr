package edu.illinois.cs.cogcomp.esrl.latent.cache;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.esrl.Properties;
import edu.illinois.cs.cogcomp.nlp.utilities.WhiteSpaceTokenizer;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class CachedEditDistanceScorer implements ICachedScorer {
    private Map<String, Double> cache = null;
    private String cache_file_name = "EDITDISTANCE_CACHE";
    private static final String sep = "$$$$$";

    public CachedEditDistanceScorer() {
        try {
            cache_file_name = Properties.editDistanceCacheFile;
            String cacheDir = ScoreManager.cacheDir;

            if (IOUtils.exists(cacheDir + "/" + cache_file_name)) {
                System.out.println("LOAD EDIT DISTANCE cache from " + cacheDir + "/" + cache_file_name);
                ObjectInputStream ois = new ObjectInputStream(
                        new BufferedInputStream(new FileInputStream(cacheDir + "/" + cache_file_name)));
                cache = (Map<String, Double>) ois.readObject();
                ois.close();
            }
            else cache = new HashMap<>();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static double multiWordNormalizedEditDistance(String t, String h) {
        String[] tTokens = WhiteSpaceTokenizer.tokenizeToArray(t);
        String[] hTokens = WhiteSpaceTokenizer.tokenizeToArray(h);

        double bestest = Double.NEGATIVE_INFINITY;
        for (String hh : hTokens) {
            double best = Double.NEGATIVE_INFINITY;

            for (String tt : tTokens) {
                double score = normalizedEditDistance(tt, hh);
                if (score > best) {
                    best = score;
                }
            }
            bestest = Math.max(bestest, best);
        }

        if (bestest < 0.9)
            return 0;
        else
            return bestest;
    }

    public static double normalizedEditDistance(String text, String hyp) {
        text = text.toLowerCase();
        hyp = hyp.toLowerCase();

        // except the dp_path[src.length()][trg.length()]
        double dp_table[][];
        OPERATORS dp_path[][];

        int n = text.length();
        int m = hyp.length();

        dp_table = new double[text.length() + 1][];
        dp_path = new OPERATORS[text.length() + 1][];

        for (int i = 0; i <= n; i++) {
            dp_table[i] = new double[hyp.length() + 1];
            dp_path[i] = new OPERATORS[hyp.length() + 1];
            for (int j = 0; j <= m; j++)
                dp_path[i][j] = OPERATORS.NONE;
        }

        for (int i = n; i >= 0; i--) {
            for (int j = m; j >= 0; j--) {

                // starting point
                if (i == n && j == m) {
                    dp_table[i][j] = 0;
                    continue;
                }

                // else, do dynamic programming
                Vector<OPERATORS> allow_operators = new Vector<>();

                if (i == n) {
                    allow_operators.add(OPERATORS.INS);
                } else if (j == m) {
                    allow_operators.add(OPERATORS.DEL);
                } else {
                    allow_operators.add(OPERATORS.INS);
                    allow_operators.add(OPERATORS.DEL);
                    allow_operators.add(OPERATORS.REP);
                }

                double ins_cost, del_cost, rep_cost;
                ins_cost = del_cost = rep_cost = Double.NEGATIVE_INFINITY;

                for (OPERATORS op : allow_operators) {

                    if (op == OPERATORS.INS) {
                        ins_cost = -1.0 + dp_table[i][j + 1];
                    }

                    if (op == OPERATORS.DEL) {
                        del_cost = -1.0 + dp_table[i + 1][j];
                    }

                    if (op == OPERATORS.REP) {
                        double r;
                        if (text.charAt(i) == hyp.charAt(j))
                            r = 0;
                        else if ((text.charAt(i) + "").equalsIgnoreCase("" + hyp.charAt(j)))
                            r = -0.5;
                        else
                            r = -1;

                        rep_cost = r + dp_table[i + 1][j + 1];
                    }
                }

                // ugly code: fill out the best move and
                if (ins_cost >= del_cost && ins_cost >= rep_cost) {
                    dp_table[i][j] = ins_cost;
                    dp_path[i][j] = OPERATORS.INS;
                } else if (del_cost >= ins_cost && del_cost >= rep_cost) {
                    dp_table[i][j] = del_cost;
                    dp_path[i][j] = OPERATORS.DEL;
                } else {
                    dp_table[i][j] = rep_cost;
                    dp_path[i][j] = OPERATORS.REP;
                }

            }
        }

        Vector<String> res = new Vector<>();
        int cur_i = 0, cur_j = 0;

        while (cur_i != text.length() || cur_j != hyp.length()) {
            if (dp_path[cur_i][cur_j] == OPERATORS.INS) {
                res.add("*#" + hyp.charAt(cur_j));
                cur_j += 1;
            } else if (dp_path[cur_i][cur_j] == OPERATORS.DEL) {
                res.add(text.charAt(cur_i) + "#*");
                cur_i += 1;
            } else {
                res.add(text.charAt(cur_i) + "#" + hyp.charAt(cur_j));
                cur_i += 1;
                cur_j += 1;
            }
        }

        return 1.0 + (dp_table[0][0] / res.size());

    }

    static public void main(String[] args) throws Exception {
        System.out.println(multiWordNormalizedEditDistance("Neverland", "Santa Barbara County"));
    }

    public double getScore(String s1, String s2) {
        if (cache.containsKey(s1 + sep + s2)) {
            return cache.get(s1 + sep + s2);
        }
        else {
            Double d = normalizedEditDistance(s1, s2);
            cache.put(s1 + sep + s2, d);
            return d;
        }
    }

    public void save() throws IOException {
        System.out.println("Save EDIT Distanceche...");
        System.out.flush();
        ObjectOutputStream oos = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(
                        ScoreManager.cacheDir + "/" + cache_file_name)));
        oos.writeObject(cache);
        oos.close();
        System.out.println("Done...");
        System.out.flush();
    }

    enum OPERATORS {
        NONE, INS, DEL, REP
    }
}
