package edu.illinois.cs.cogcomp.esrl.data;

/**
 * Created by christod on 1/29/15.
 */
public class NoAlignmentFoundException extends Exception {

    public NoAlignmentFoundException(int textPreds, int hypPreds) {
        super("No alignment found. Text had " + textPreds + " predicates, Hypothesis had " + hypPreds);
    }
}
