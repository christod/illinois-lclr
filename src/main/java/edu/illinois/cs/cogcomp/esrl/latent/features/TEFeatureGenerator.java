package edu.illinois.cs.cogcomp.esrl.latent.features;

import edu.illinois.cs.cogcomp.comma.CommaLabeler;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.POSUtils;
import edu.illinois.cs.cogcomp.esrl.Properties;
import edu.illinois.cs.cogcomp.esrl.latent.cache.ICachedScorer;
import edu.illinois.cs.cogcomp.esrl.latent.cache.ScoreManager;

import java.util.HashMap;
import java.util.Map;


public class TEFeatureGenerator {

    public Map<String, Double> getEdgeAlignmentFeatures(Relation textEdge, Relation hypEdge) {
        Map<String, Double> map = new HashMap<>();

        String textPredicate = textEdge.getSource().getSurfaceString();
        String textArgument = textEdge.getTarget().getSurfaceString();
        String hypothesisPredicate = hypEdge.getSource().getSurfaceString();
        String hypothesisArgument = hypEdge.getTarget().getSurfaceString();

        String textEdgeLabel = textEdge.getRelationName();
        String hypEdgeLabel = hypEdge.getRelationName();
        String textEdgeLabelInfo = getLabelInfo(textEdgeLabel);
        String hypEdgeLabelInfo = getLabelInfo(hypEdgeLabel);

        double predicateMatchScore = ScoreManager.llm.getScore(textPredicate, hypothesisPredicate);
        double argumentMatchScore = ScoreManager.llm.getScore(textArgument, hypothesisArgument);
        double argumentMatchNEScore = ScoreManager.ne.getScore(textArgument, hypothesisArgument);

        double conjunctionScore = Math.sqrt(predicateMatchScore * argumentMatchScore);
        map.put("edge-conjunction-score", conjunctionScore);

        if (Properties.extendedAlignFeatures) {
            if (textEdgeLabel.equals(hypEdgeLabel))
                map.put("edge-conjunction-score-label-match-" + getLabelInfo(textEdgeLabel), conjunctionScore);
            else
                map.put("edge-label-mismatch-text?" + textEdgeLabelInfo + "-hyp?" + hypEdgeLabelInfo, 1.0);

            if (predicateMatchScore == 0 && !textEdgeLabelInfo.equals(hypEdgeLabelInfo)) {
                double matchScore = getSimilarityAcrossArgs(textEdge.getSource(), hypEdge.getSource(), ScoreManager.llm);
                double matchNEScore = getSimilarityAcrossArgs(textEdge.getSource(), hypEdge.getSource(), ScoreManager.ne);
                if (matchScore > 0)
                    map.put("edge-different-views-args-match", matchScore * argumentMatchScore);
                else if (matchNEScore > 0)
                    map.put("edge-different-views-args-ne-match", matchNEScore * argumentMatchNEScore);
            }
        }
        return map;
    }

    private double getSimilarityAcrossArgs(Constituent textPred, Constituent hypPred, ICachedScorer scorer) {
        double argsMatch = 0;
        int maxArgs = Math.max(textPred.getOutgoingRelations().size(), hypPred.getOutgoingRelations().size());
        double totalScore = 0;
        for (Relation textRel : textPred.getOutgoingRelations()) {
            String textArgument = textRel.getTarget().getSurfaceString();
            for (Relation hypRel : hypPred.getOutgoingRelations()) {
                String hypothesisArgument = hypRel.getTarget().getSurfaceString();
                double argumentMatchScore = scorer.getScore(textArgument, hypothesisArgument);
                if (argumentMatchScore != 0) {
                    argsMatch++;
                    totalScore += argumentMatchScore;
                }
            }
        }
        if (argsMatch == 0) return 0;
        return (totalScore/argsMatch)*(argsMatch/maxArgs);
    }

    private String getLabelInfo(String label) {
        if (label.contains("Parent:"))
            return "dep";
        else if (label.contains(ViewNames.SRL_PREP))
            return "srl-p";
        else if (label.contains(ViewNames.SRL_NOM))
            return "srl-n";
        else if (label.contains(ViewNames.SRL_VERB))
            return "srl-v";
        else if (label.contains(CommaLabeler.VIEW_NAME))
            return "srl-c";
        return "other";
    }

    public Map<String, Double> getNodeAlignmentFeatures(Constituent textNode, Constituent hypNode) {
        Map<String, Double> map = new HashMap<>();

        map.put("*node-bias*", 1.0);

        String textSurfaceString = textNode.getSurfaceString();
        String hypSurfaceString = hypNode.getSurfaceString();

        double wnsimScore = ScoreManager.wsim.getScore(textSurfaceString, hypSurfaceString);

        map.put("node-pair-with-coref-wnsim", TEFeatureHelper.getNodePairScore(textNode, hypNode));

        if (wnsimScore < 0)
            map.put("node-surface-wnsim-negative", 1.0);

        boolean text_neg = false;
        boolean hyp_neg = false;

        if (textSurfaceString.equals("not"))
            text_neg = true;

        if (hypSurfaceString.equals("not"))
            hyp_neg = true;

        if (hyp_neg != text_neg)
            map.put("node-Not-mismatch", 1.0);

        text_neg = false;
        hyp_neg = false;

        for (Relation r : textNode.getIncomingRelations()) {
            if (r.getRelationName().contains("NEG"))
                text_neg = true;
        }

        for (Relation r : hypNode.getIncomingRelations()) {
            if (r.getRelationName().contains("NEG"))
                hyp_neg = true;
        }

        if (hyp_neg != text_neg)
            map.put("node-Incoming-neg-mismatch", 1.0);

        if (textNode.getLabel().equals("Predicate") && hypNode.getLabel().equals("Predicate"))
            map.put("node-both-predicates", 1.0);

        // POS based features
        String textNodePOS = TEFeatureHelper.getPOS(textNode);
        String hypNodePOS = TEFeatureHelper.getPOS(hypNode);

        if (POSUtils.isPOSClosedSet(textNodePOS) && POSUtils.isPOSClosedSet(hypNodePOS))
            map.put("node-both-pos-closed-set", 1.0);

        if (textNode.getNumberOfTokens() == 1 && hypNode.getNumberOfTokens() == 1 && hypNodePOS.equals(textNodePOS)) {
            map.put("node-same-POS_h-" + hypNodePOS.substring(0, 1), 1.0);
        }
        else {
            map.put("node-diff-POS_h-" + hypNodePOS.substring(0, 1), 1.0);
        }

        // NE based feature
        double edScore = ScoreManager.ed.getScore(textSurfaceString, hypSurfaceString);

        if (textNodePOS.substring(0, 1).equals("V") && hypNodePOS.substring(0, 1).equals("N")
                || textNodePOS.substring(0, 1).equals("N") && hypNodePOS.substring(0, 1).equals("V"))
            map.put("node-verb-noun-align", edScore);


        if (Properties.extendedAlignFeatures) {
            if (textNode.getLabel().equals("Predicate") && hypNode.getLabel().equals("Predicate")) {
                if (textNode.getAttribute(CoNLLColumnFormatReader.SenseIdentifer).equals(
                        hypNode.getAttribute(CoNLLColumnFormatReader.SenseIdentifer)))
                    map.put("node-predicate-sense-match", 1.0);
            }
        }
        return map;
    }


}
