package edu.illinois.cs.cogcomp.esrl.latent.cache;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.esrl.Annotator;
import edu.illinois.cs.cogcomp.esrl.Properties;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class CachedWNSIMScorer implements ICachedScorer {
    private Map<String, Double> cache = null;
    private String cache_file_name = "WNSIM_CACHE";
    private static final String sep = "$$$$$";
    private Annotator annotator;

    private XmlRpcClient client;

    public CachedWNSIMScorer() {
        try {
            this.annotator = Annotator.getInstance();
            cache_file_name = Properties.wnSimCacheFile;
            String cacheDir = ScoreManager.cacheDir;

            if (IOUtils.exists(cacheDir + "/" + cache_file_name)) {
                System.out.println("LOAD WSIMcache from " + cacheDir + "/" + cache_file_name);
                ObjectInputStream ois = new ObjectInputStream(
                        new BufferedInputStream(new FileInputStream(cacheDir + "/" + cache_file_name)));
                cache = (Map<String, Double>) ois.readObject();
                ois.close();
            }
            else cache = new HashMap<>();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        /*try {
            URL wnSimServer = new URL("http://greedy.cs.illinois.edu:29022");
            XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
            config.setServerURL(wnSimServer);
            client = new XmlRpcClient();
            client.setConfig(config);
        } catch (MalformedURLException var1) {
            System.err.println("Invalid WNSimServer");
        }*/
    }

    public double getScore(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        boolean num1 = true;
        boolean num2 = true;
        double d1 = 0, d2 = 0;
        try {
            d1 = Double.parseDouble(s1);
        } catch (NumberFormatException e) {
            num1 = false;
        }

        try {
            d2 = Double.parseDouble(s2);
        } catch (NumberFormatException e) {
            num2 = false;
        }

        if (s1.equalsIgnoreCase("__NULL__W") || s2.equalsIgnoreCase("__NULL__W"))
            return 0;
        else if (num1 != num2)
            return 0;
        else if (num1 && (d1 != d2))
            return 0;
        else if (cache.containsKey(s1 + sep + s2))
            return cache.get(s1 + sep + s2);
        else if (cache.containsKey(s2 + sep + s1))
            return cache.get(s2 + sep + s1);
        else {
            Double dLocal = annotator.getWNSim(s1, s2);
            //Double dServer = getSimilarity(s1, s2);
            cache.put(s1 + sep + s2, dLocal);
            return dLocal;
        }
    }

    public void save() throws IOException {
        System.out.println("Save WSIM cache...");
        System.out.flush();
        ObjectOutputStream oos = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(ScoreManager.cacheDir + "/" + cache_file_name)));
        oos.writeObject(cache);
        oos.close();
        System.out.println("Done...");
        System.out.flush();
    }

    public double getSimilarity(String t1, String t2) {
        Hashtable<String, String> s = new Hashtable<>();
        s.put("FIRST_STRING", t1);
        s.put("SECOND_STRING", t2);
        Object[] params = new Object[]{s};
        HashMap result = null;
        try {
            result = (HashMap) client.execute("WNSim", params);
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return new Double(result.get("SCORE").toString());
    }
}
