package edu.illinois.cs.cogcomp.esrl.latent.views;

import edu.illinois.cs.cogcomp.comma.CommaLabeler;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.features.helpers.ParseHelper;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.edison.utilities.*;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SemanticTree extends View implements ISemanticDAG {
    public static final String SRL_PREDICATE_ID = "SRL_PREDICATE_ID";
    public static final String SRL_PREDICATE_SENSE = "SRL_PREDICATE_SENSE";
    public static final String SRL_ARGUMENT_LABEL = "SRL_ARGUMENT_LABEL";
    private static final long serialVersionUID = -5207738129418869221L;

    protected Set<Integer> ignoredTokens;

    protected Constituent root;

    public SemanticTree(String viewName, String viewGenerator, TextAnnotation text, double score) {
        super(viewName, viewGenerator, text, score);
        generateViewData();
    }

    public SemanticTree(TextAnnotation text) {
        this("SemanticTreeView", "Default", text, 1d);
    }

    protected void generateViewData() {
        makeRootConstituent();

        TextAnnotation ta = getTextAnnotation();

        ignoredTokens = new TreeSet<>();

        PredicateArgumentView verbSRLView = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
        for (Constituent predicate : verbSRLView.getPredicates()) {
            addPredicate(predicate, verbSRLView);
        }
        PredicateArgumentView nomSRLView = (PredicateArgumentView) ta.getView(ViewNames.SRL_NOM);
        for (Constituent predicate : nomSRLView.getPredicates()) {
            addPredicate(predicate, nomSRLView);
        }
        PredicateArgumentView prepSRLView = (PredicateArgumentView) ta.getView(ViewNames.SRL_PREP);
        for (Constituent predicate : prepSRLView.getPredicates()) {
            addPredicate(predicate, prepSRLView);
        }
        PredicateArgumentView commaSRLView = (PredicateArgumentView) ta.getView(CommaLabeler.VIEW_NAME);
        for (Constituent predicate : commaSRLView.getPredicates()) {
            addPredicate(predicate, commaSRLView);
        }
    }

    protected void makeRootConstituent() {
        root = new Constituent("TOP", 1, "SemanticTree", getTextAnnotation(), 0, getTextAnnotation().size());
        addConstituent(root);
    }

    protected void addPredicate(Constituent predicate, PredicateArgumentView srlView) {
        // First add the predicate. Predicates can be multiple tokens/words.
        Constituent predicateNode = createConstituent(predicate, true);
        String predicateNodeId = predicateNode.toString() + "-" + predicateNode.getSpan();
        predicateNode.addAttribute(SRL_PREDICATE_SENSE, predicate.getAttribute(CoNLLColumnFormatReader.SenseIdentifer));
        predicateNode.addAttribute(SRL_PREDICATE_ID, predicateNodeId);
        addConstituent(predicateNode);

        addEdge(root, predicateNode, "Predicate");

        for (Relation srlRelation : srlView.getArguments(predicate)) {
            Constituent arg = srlRelation.getTarget();
            String argViewName = arg.getViewName();
            // XXX A fix for prepSRL objects; converts them from single words to constituents
            if (argViewName.equals(ViewNames.SRL_PREP) && srlRelation.getRelationName().equals("Object")) {
                Constituent argPhrase = ParseHelper.getPhraseFromHead(predicate, arg, ViewNames.PARSE_STANFORD);
                if (argPhrase == null) continue;
                arg = argPhrase;
            }
            String relationName = srlView.getViewName() + ":" + srlRelation.getRelationName();

            Constituent argNode = createConstituent(arg, true);
            if (!containsConstituent(argNode)) {
                argNode.addAttribute(SRL_PREDICATE_ID, predicateNodeId);
                //TODO Maybe replace A0-4 etc. with Propbank labels?
                argNode.addAttribute(SRL_ARGUMENT_LABEL, relationName);

                addConstituent(argNode);
            }
            addEdge(predicateNode, argNode, relationName);
        }
    }

    protected void addEdge(Constituent parent, Constituent child, String label) {
        addRelation(new Relation(label, parent, child, 1d));
    }

    protected Constituent createConstituent(Constituent constituent, boolean singleToken) {
        if (singleToken) {
            try {
                CollinsHeadFinder headFinder = new CollinsHeadFinder();
                int headPosition = ParseHelper.getHeadWordPosition(constituent, headFinder, ViewNames.PARSE_STANFORD);
                String headWord = WordHelpers.getWord(constituent.getTextAnnotation(), headPosition);
                return new Constituent(headWord, constituent.getConstituentScore(),
                        this.getViewName(), this.getTextAnnotation(), headPosition, headPosition + 1);

            } catch (EdisonException e) {
                e.printStackTrace();
            }
        }

        return new Constituent(constituent.getLabel(),
                constituent.getConstituentScore(), this.getViewName(), this.getTextAnnotation(),
                constituent.getStartSpan(), constituent.getEndSpan());
    }

    /**
     * Override of the original method to ensure equivalence of newly created
     * constituents that cover the same span and have the same label.
     * @param c Constituent to check
     * @return Whether the constituent already exists in the DAG
     */
    @Override
    public boolean containsConstituent(Constituent c) {
        if (super.containsConstituent(c)) return true;
        for (Constituent thisC : getConstituents()) {
            if (c.getLabel().equals(thisC.getLabel()) && c.getSpan().equals(thisC.getSpan()))
                return true;
        }
        return false;
    }

    public List<Relation> getEdges() {
        return this.getRelations();
    }

    public List<Constituent> getNodes() {
        return this.getConstituents();
    }

    public Constituent getRoot() {
        return this.root;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Relation predicateRelation : root.getOutgoingRelations()) {
            sb.append("Predicate:\n");
            sb.append(predicateRelation.getTarget().toSExpression());
            sb.append("\n\n");
        }

        return sb.toString();
    }
}
