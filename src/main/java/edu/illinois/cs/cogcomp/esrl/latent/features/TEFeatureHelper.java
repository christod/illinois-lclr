package edu.illinois.cs.cogcomp.esrl.latent.features;

import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.esrl.latent.cache.ScoreManager;

import java.util.List;

public class TEFeatureHelper {
    public static String getPOS(Constituent c) {
        return ((TokenLabelView) c.getTextAnnotation().getView("POS")).getLabel(c.getEndSpan() - 1);
    }

    private static boolean isNoun(Constituent t) {
        return getPOS(t).substring(0, 1).equals("N");
    }

    public static boolean isPronoun(Constituent t) {
        return getPOS(t).equals("WP")
                || getPOS(t).equals("WP$")
                || getPOS(t).equals("PRP")
                || getPOS(t).equals("PRP$");
    }

    private static boolean isCorefTarget(Constituent t) {
        if (t.getSurfaceString().equals("US"))
            return false;

        if (isPronoun(t))
            return true;
        String s = t.getSurfaceString();
        return s.equalsIgnoreCase("who") || s.equalsIgnoreCase("which");
    }

    private static boolean isSelfSurfix(Constituent t) {
        String s = t.getSurfaceString();
        return s.length() >= 4 && s.substring(s.length() - 4, s.length()).equals("self");
    }

    private static double getCorefScore(Constituent t, Constituent h) {
        boolean run_coref = false;
        if (isCorefTarget(t) && isNoun(h))
            run_coref = true;

        if (isCorefTarget(h) && isNoun(t))
            run_coref = true;

        if (!run_coref)
            return 0.0;

        Constituent textT = getCorefCannonicalSurfaceConstituent(t);
        Constituent textH = getCorefCannonicalSurfaceConstituent(h);

        boolean coref_success = true;

        if (textT == null || textH == null)
            coref_success = false;

        if (coref_success && isCorefTarget(t) && textT.getSurfaceString().equals(t.getSurfaceString()))
            coref_success = false;

        if (coref_success && isCorefTarget(h) && textH.getSurfaceString().equals(h.getSurfaceString()))
            coref_success = false;

        if (coref_success && (isSelfSurfix(textH) || isSelfSurfix(textT)))
            coref_success = false;

        if (!coref_success) {
            return 0.0;
        }

        String[] h_s = textH.getSurfaceString().split("\\s+");
        String h_head_s = h_s[h_s.length - 1];

        String[] t_s = textT.getSurfaceString().split("\\s+");
        String t_head_s = t_s[t_s.length - 1];

        return ScoreManager.wsim.getScore(t_head_s, h_head_s);
    }

    static double getNodePairScore(Constituent textNode, Constituent hypNode) {
        String textSurfaceString = textNode.getSurfaceString();
        String hypSurfaceString = hypNode.getSurfaceString();

        double wnsimScore = ScoreManager.wsim.getScore(textSurfaceString, hypSurfaceString);
        return Math.max(Math.abs(wnsimScore), Math.abs(getCorefScore(textNode, hypNode)));
    }

    private static Constituent getCorefCannonicalSurfaceConstituent(Constituent c) {
        if (isPronoun(c)) {
            if (!c.getTextAnnotation().hasView(ViewNames.COREF))
                return null;

            TextAnnotation textAnnotation = c.getTextAnnotation();

            CoreferenceView corefView = (CoreferenceView) textAnnotation.getView(ViewNames.COREF);
            List<Constituent> corefConstituents = corefView.getConstituentsCovering(c);

            if (corefConstituents.size() == 0)
                return null;

            Constituent firstConstituent = corefConstituents.get(0);
            return corefView.getCanonicalEntity(firstConstituent);
        }
        else {
            return c;
        }
    }
}
