package edu.illinois.cs.cogcomp.esrl;

import edu.illinois.cs.cogcomp.cachingcurator.CachingAnnotator;
import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.entitySimilarity.compare.EntityComparison;
import edu.illinois.cs.cogcomp.esrl.data.CommaViewGenerator;
import edu.illinois.cs.cogcomp.esrl.data.QuantitiesViewGenerator;
import edu.illinois.cs.cogcomp.esrl.data.DAGViewGenerator;
import edu.illinois.cs.cogcomp.esrl.data.HeadFinderViewGenerator;
import edu.illinois.cs.cogcomp.llm.comparators.LlmStringComparator;
import edu.illinois.cs.cogcomp.quant.driver.Quantifier;
import edu.illinois.cs.cogcomp.sim.WNSim;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all annotators required for pre-processing and training.
 */
public class Annotator {
    private Quantifier quantifier;
    private CachingCurator preprocessor;
    private LlmStringComparator llm;
    private EntityComparison neComp;
    private WNSim wnSim;

    private static Annotator instance;

    public Annotator() {
        // A map containing View names and whether to force update
        Map<String, Boolean> requiredViews = new HashMap<>();
        requiredViews.put(ViewNames.LEMMA, false);
        requiredViews.put(ViewNames.POS, false);
        requiredViews.put(ViewNames.SHALLOW_PARSE, false);
        requiredViews.put(ViewNames.PARSE_CHARNIAK, false);
        requiredViews.put(ViewNames.PARSE_STANFORD, false);
        requiredViews.put(ViewNames.NER, false);
        requiredViews.put(ViewNames.COREF, false);
        requiredViews.put(ViewNames.SRL_VERB, false);
        requiredViews.put(ViewNames.SRL_NOM, false);
        requiredViews.put(ViewNames.SRL_PREP, false);

        // Add the external annotators
        Map<String, CachingAnnotator> externalAnnotators = new HashMap<>();
        DAGViewGenerator dagViewGenerator = new DAGViewGenerator();
        QuantitiesViewGenerator quantitiesViewGenerator = new QuantitiesViewGenerator();
        HeadFinderViewGenerator headFinderViewGenerator = new HeadFinderViewGenerator();
        CommaViewGenerator commaViewGenerator = new CommaViewGenerator();
        //externalAnnotators.put(dagViewGenerator.getViewName(), dagViewGenerator);
        //externalAnnotators.put(ViewNames.QUANTITIES, quantitiesViewGenerator);
        externalAnnotators.put(headFinderViewGenerator.getViewName(), headFinderViewGenerator);
        externalAnnotators.put(commaViewGenerator.getViewName(), commaViewGenerator);
        try {
            ResourceManager rm = new ResourceManager("config/e-srl.properties");
            preprocessor = new CachingCurator(rm.getCuratorHost(), rm.getCuratorPort(),
                    requiredViews, false, externalAnnotators);
            // Set the location for CachingCurator's cache
            CachingCurator.setCacheDiskStoreDir(rm.getString("cachingCuratorStoreDir"));
            neComp = new EntityComparison();
            llm = new LlmStringComparator("config/llm.properties");
            wnSim = new WNSim("data/WordNet");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Annotator getInstance() {
        if (instance == null) {
            instance = new Annotator();
        }
        return instance;
    }

    public TextAnnotation preProcessText(String text) {
        return preprocessor.getCachedTextAnnotation(text);
    }

    public double getLLM(String string1, String string2) {
        try {
            double v = llm.compareStrings(string1, string2);
            if (Double.isNaN(v)) return 0;
            return v;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public double getNESim(String entity1, String entity2) {
        String scoreStr = neComp.compareNames(entity1, entity2).get("SCORE");
        return Double.parseDouble(scoreStr);
    }

    public double getWNSim(String string1, String string2) {
        return wnSim.score(string1, string2);
    }

    public double compareQuantities(String string1, String string2) {
        String string1Quants = quantifier.getAnnotatedString(string1, true);
        String string2Quants = quantifier.getAnnotatedString(string2, true);
        // TODO compare the quantities
        return 0.0;
    }
}
