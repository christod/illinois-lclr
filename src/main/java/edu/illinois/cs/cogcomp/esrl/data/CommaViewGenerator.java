package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.cachingcurator.CachingAnnotator;
import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.comma.CommaLabeler;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;
import org.apache.thrift.TException;

/**
 * A wrapper for the {@link CommaLabeler}.
 */
public class CommaViewGenerator implements CachingAnnotator {
    private CommaLabeler commaLabeler;
    private String[] requiredViews;

    public CommaViewGenerator() {
        commaLabeler = new CommaLabeler();
        requiredViews = commaLabeler.getRequiredViews();
    }

    @Override
    public View getView(TextAnnotation ta, CachingCurator cachingCurator)
            throws TException, AnnotationFailedException, ServiceUnavailableException {
        // First, make sure we have all the required views
        for (String view : requiredViews) {
            if (!ta.hasView(view)) cachingCurator.requestView(ta, view);
        }
        return commaLabeler.getCommaSRL(ta);
    }

    @Override
    public String[] getRequiredViews() {
        return requiredViews;
    }

    @Override
    public String getViewName() {
        return CommaLabeler.VIEW_NAME;
    }

    @Override
    public View getView(TextAnnotation textAnnotation) {
        return null;
    }
}
