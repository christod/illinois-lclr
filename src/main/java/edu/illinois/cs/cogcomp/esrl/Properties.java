package edu.illinois.cs.cogcomp.esrl;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;

import java.io.IOException;

public class Properties {
    public static String wnSimCacheFile;
    public static String llmCacheFile;
    public static String editDistanceCacheFile;
    public static String cacheDirectory;
    public static String neQuantitiesCacheFile;
    public static String wnData;

    public static boolean extendedAlignFeatures;

    public static void readProperties(String propertiesFile) throws IOException {
        ResourceManager configFile = new ResourceManager(propertiesFile);

        wnData = configFile.getString("WNData");
        wnSimCacheFile = configFile.getString("WNSimCacheFile");
        llmCacheFile = configFile.getString("LLMCacheFile");
        editDistanceCacheFile = configFile.getString("EditDistanceCacheFile");
        neQuantitiesCacheFile = configFile.getString("NEQuantitiesCacheFile");
        cacheDirectory = configFile.getString("CacheDir");
        extendedAlignFeatures = configFile.getBoolean("ExtendedAlignmentFeatures");
    }
}
