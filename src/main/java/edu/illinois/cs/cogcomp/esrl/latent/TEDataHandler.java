package edu.illinois.cs.cogcomp.esrl.latent;


import edu.illinois.cs.cogcomp.esrl.data.ACL10Reader;
import edu.illinois.cs.cogcomp.esrl.data.TEPair;

import java.util.ArrayList;
import java.util.List;

public class TEDataHandler {
    public static final String ENTAILMENT = "ENTAILMENT";
    public static final String CONTRADICTION = "CONTRADICTION";
    public static final String UNKNOWN = "UNKNOWN";

    public static List<TEPair> readRaterTEData(String fileName) throws Exception {
        ACL10Reader dataReader = new ACL10Reader(fileName);
        List<TEPair> list = new ArrayList<>();
        TEPair pair;
        int counter = 1;
        while ((pair = dataReader.next()) != null) {
            list.add(pair);
            if (counter % 50 == 0)
                System.out.println("Processed " + counter + " pairs");
            counter++;
        }
        return list;
    }
}
