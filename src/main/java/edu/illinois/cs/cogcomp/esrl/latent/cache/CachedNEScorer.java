package edu.illinois.cs.cogcomp.esrl.latent.cache;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.esrl.Annotator;
import edu.illinois.cs.cogcomp.esrl.Properties;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class CachedNEScorer implements ICachedScorer {
    private Map<String, Double> cache = null;
    private String cache_file_name = "NESIM_CACHE";
    private static final String sep = "$$$$$";
    private Annotator annotator;

    public CachedNEScorer() {
        try {
            this.annotator = Annotator.getInstance();
            cache_file_name = Properties.neQuantitiesCacheFile;
            String cacheDir = ScoreManager.cacheDir;

            if (IOUtils.exists(cacheDir + "/" + cache_file_name)) {
                System.out.println("LOAD NE cache from " + cacheDir + "/" + cache_file_name);
                ObjectInputStream ois = new ObjectInputStream(
                        new BufferedInputStream(new FileInputStream(cacheDir + "/" + cache_file_name)));
                cache = (Map<String, Double>) ois.readObject();
                ois.close();
            }
            else cache = new HashMap<>();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public double getScore(String s1, String s2) {
        if (cache.containsKey(s1 + sep + s2))
            return cache.get(s1 + sep + s2);
        else if (cache.containsKey(s2 + sep + s1))
            return cache.get(s2 + sep + s1);
        else {
            Double d = annotator.getNESim(s1, s2);
            cache.put(s1 + sep + s2, d);
            return d;
        }
    }

    public void save() throws IOException {
        System.out.println("Save NE cache...");
        System.out.flush();
        ObjectOutputStream oos = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(
                        ScoreManager.cacheDir + "/" + cache_file_name)));
        oos.writeObject(cache);
        oos.close();
        System.out.println("Done...");
        System.out.flush();
    }
}
