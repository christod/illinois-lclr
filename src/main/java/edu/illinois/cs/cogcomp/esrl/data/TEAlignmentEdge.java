package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;

import java.util.ArrayList;
import java.util.List;

/**
 * An slightly abstract notion of constituent that captures a variety of phenomena like SRL pred/args, chunks,
 * parse subtrees or individual words. The underlying representation is simply a series of {@link IntPair}s
 * that represent the start and end of (non-continuous) constituents.
 */
public class TEAlignmentEdge {
    private TextAnnotation text, hypothesis;
    private List<IntPair> textSpans, hypothesisSpans;
    private List<String> textLabels, hypothesisLabels;
    private List<Double> scores;

    public TEAlignmentEdge(TextAnnotation text, TextAnnotation hypothesis) {
        this.text = text;
        this.textSpans = new ArrayList<>();
        this.textLabels = new ArrayList<>();
        this.hypothesis = hypothesis;
        this.hypothesisSpans = new ArrayList<>();
        this.hypothesisLabels = new ArrayList<>();
        this.scores = new ArrayList<>();
    }

    public TEAlignmentEdge(TEAlignmentEdge other) {
        this.text = other.text;
        this.textSpans = new ArrayList<>(other.textSpans);
        this.textLabels = new ArrayList<>(other.textLabels);
        this.hypothesis = other.hypothesis;
        this.hypothesisSpans = new ArrayList<>(other.hypothesisSpans);
        this.hypothesisLabels = new ArrayList<>(other.hypothesisLabels);
        this.scores = new ArrayList<>(other.scores);
    }

    /**
     * Appends another edge to the current one
     * @param other The alignment edge to be added
     */
    public void addAlignmentEdge(TEAlignmentEdge other) {
        // The TextAnnotation objects should be the same
        if (! (this.text.equals(other.text) || this.hypothesis.equals(other.hypothesis))) {
            System.err.println("Cannot merge edges from different TextAnnotations");
            // Too drastic; in the future maybe just throw an exception
            System.exit(-1);
        }
        textSpans.addAll(other.textSpans);
        textLabels.addAll(other.textLabels);
        hypothesisSpans.addAll(other.hypothesisSpans);
        hypothesisLabels.addAll(other.hypothesisLabels);
        scores.addAll(other.scores);
    }

    public void addComponent(IntPair textSpan, String textSpanLabel,
                             IntPair hypothesisSpan, String hypothesisSpanLabel,
                             double score) {
        boolean addSpans = true;
        // Enforce the unique Text span constraint
        // Check if the Text span has been aligned to anything previously
        // If it has, keep the one with the best score
        int i = textSpans.indexOf(textSpan);
        if (i > -1) addSpans = score > scores.get(i);
        if (addSpans) {
            textSpans.add(textSpan);
            textLabels.add(textSpanLabel);
            hypothesisSpans.add(hypothesisSpan);
            hypothesisLabels.add(hypothesisSpanLabel);
            scores.add(score);
        }
    }

    /**
     * Calculates the score of all sub-constituents. Right now it's the average,
     * but maybe we need to use a better function.
     * @return The final score across all sub-constituents
     */
    public double getFinalScore() {
        double finalScore = 0;
        for (Double score : scores)
            finalScore += score;
        return finalScore/scores.size();
    }

    public List<IntPair> getTextSpans() {
        return textSpans;
    }

    public List<IntPair> getHypothesisSpans() {
        return hypothesisSpans;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        // All the lists should be of equal size
        for (int i = 0; i < scores.size(); i++) {
            IntPair textSpan = textSpans.get(i);
            IntPair hypothesisSpan = hypothesisSpans.get(i);
            sb.append(textLabels.get(i)).append("[");
            sb.append(arrayToText(text.getTokensInSpan(textSpan.getFirst(), textSpan.getSecond())));
            sb.append("]");
            sb.append("->");
            sb.append(hypothesisLabels.get(i)).append("[");
            sb.append(arrayToText(hypothesis.getTokensInSpan(hypothesisSpan.getFirst(), hypothesisSpan.getSecond())));
            sb.append("]");
            sb.append("\n");
        }
        return sb.toString();
    }

    private String arrayToText(String[] array) {
        String string = "";
        for (String s : array) string += s + " ";
        return string.trim();
    }
}
