package edu.illinois.cs.cogcomp.esrl.latent.cache;

import edu.illinois.cs.cogcomp.esrl.Properties;

import java.io.IOException;

public class ScoreManager {
    public static String cacheDir = Properties.cacheDirectory;
    public static ICachedScorer ed = new CachedEditDistanceScorer();
    public static ICachedScorer wsim = new CachedWNSIMScorer();
    public static ICachedScorer llm = new CachedLLMScorer();
    public static ICachedScorer ne = new CachedNEScorer();

    public static void save() throws IOException {
        wsim.save();
        llm.save();
        ed.save();
        ne.save();
    }
}
