package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.cachingcurator.CachingAnnotator;
import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.quant.driver.Quantifier;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;
import org.apache.thrift.TException;

/**
 * A wrapper for the illinois-quantifier
 */
public class QuantitiesViewGenerator implements CachingAnnotator {
    private Quantifier quantifier = new Quantifier();

    @Override
    public View getView(TextAnnotation ta, CachingCurator cachingCurator) throws TException,
            AnnotationFailedException, ServiceUnavailableException {
        cachingCurator.requestView(ta, ViewNames.SENTENCE);
        quantifier.addQuantifierView(ta);
        return ta.getView(ViewNames.QUANTITIES);
    }

    @Override
    public String[] getRequiredViews() {
        return new String[]{ViewNames.SENTENCE};
    }

    @Override
    public String getViewName() {
        return ViewNames.QUANTITIES;
    }

    @Override
    public View getView(TextAnnotation ta) {
        return null;
    }
}
