package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.esrl.Annotator;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ACL10Reader implements Parser {
    private List<String> textStringsList, hypothesisStringsList, labels;
    private static int counter, totalPairs;
    private static Annotator annotator;

    public ACL10Reader(String file) throws Exception {
        annotator = new Annotator();
        textStringsList = new ArrayList<>();
        hypothesisStringsList = new ArrayList<>();
        labels = new ArrayList<>();
        counter = 0;
        try {
            for (String line : LineIO.read(file)) {
                line = line.trim();
                if (line.startsWith("<t>"))
                    textStringsList.add(cleanup(line.substring(3, line.indexOf("</t>"))));
                else if (line.startsWith("<h>"))
                    hypothesisStringsList.add(cleanup(line.substring(3, line.indexOf("</h>"))));
                else if (line.startsWith("<pair")) {
                    Matcher m = Pattern.compile(".*entailment=\"([A-Z]+)\".*").matcher(line);
                    if (m.matches())
                        labels.add(m.group(1));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Make sure we got the same number of T/H strings and pairs
        if (textStringsList.size() != hypothesisStringsList.size() || textStringsList.size() != labels.size()) {
            System.err.println("Error reading the corpus; not the same number of texts/hypotheses");
            System.exit(-1);
        }
        totalPairs = textStringsList.size();
    }

    @Override
    public TEPair next() {
        if (counter < totalPairs) {
            TEPair p;
            try {
                p = new TEPair(
                        Integer.toString(counter+1),
                        textStringsList.get(counter),
                        hypothesisStringsList.get(counter),
                        labels.get(counter),
                        annotator);
            } catch (AnnotationFailedException ignored) {
                System.err.println("Annotation error, moving on");
                counter++;
                return next();
            } catch (NoAlignmentFoundException e) {
                System.err.println("Alignment error, moving on");
                counter++;
                return next();
            }
            counter++;
            return p;
        }
        return null;
    }

    private String cleanup(String string) {
        String clean = string.replaceAll("&quot;", "\"");
        clean = clean.replaceAll("&amp;","&");
        clean = clean.replaceAll("\\[","(");
        clean = clean.replaceAll("\\]",")");
        clean = clean.replaceAll("£","PND ");
        clean = clean.replaceAll("€","EUR ");
        // Unicode ― and — characters
        clean = clean.replaceAll("\u2014", "--");
        clean = clean.replaceAll("\u2015", "--");
        clean = clean.replaceAll("\u00BD", "1/2");
        // Unicode no-break space character‎
        clean = clean.replaceAll("\u00A0"," ");
        // Unicode ø character
        clean = clean.replaceAll("\u00F8", "o");
        // Unicode ° (degrees) character
        clean = clean.replaceAll("\u00B0", "o");
        clean = clean.replaceAll("\\. \\.", ".");
        clean = clean.replaceAll("\\? \\.", "?");
        clean = clean.replaceAll("\\.\\) \\.", ".)");
        clean = clean.replaceAll("\\.\\(", ". (");
        clean = clean.replaceAll("\\('", "( '");
        // Fix unicode diacritics
        clean = StringUtils.normalizeUnicodeDiacritics(clean);
        // A very simple URL catcher
        clean = clean.replaceAll("(https?://)?(www\\.)?[a-z]+\\.[a-z]+(/[a-z]+)?", "URL");
        return clean;
    }

    @Override
    public void reset() {
        counter = 0;
    }

    @Override
    public void close() {}
}
