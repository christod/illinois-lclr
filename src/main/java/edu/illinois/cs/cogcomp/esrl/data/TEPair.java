package edu.illinois.cs.cogcomp.esrl.data;

import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.esrl.Annotator;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;

public class TEPair {
    private String id;
    private TextAnnotation text, hypothesis;
    private String label;

    public TEPair(String id, String textString, String hypothesisString, String label, Annotator annotator)
            throws AnnotationFailedException, NoAlignmentFoundException {
        this.id = id;
        this.text = annotator.preProcessText(textString);
        if (!DAGViewGenerator.hasRequiredAnnotations(text)) {
            throw new AnnotationFailedException();
        }
        text.addView(new DAGViewGenerator());

        this.hypothesis = annotator.preProcessText(hypothesisString);
        if (!DAGViewGenerator.hasRequiredAnnotations(hypothesis))
            throw new AnnotationFailedException();
        hypothesis.addView(new DAGViewGenerator());

        this.label = label;
    }

    public TEPair(String id, TextAnnotation text, TextAnnotation hypothesis, String label) {
        this.id = id;
        this.text = text;
        this.hypothesis = hypothesis;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getId() {
        return id;
    }

    public TextAnnotation getText() {
        return text;
    }

    public TextAnnotation getHypothesis() {
        return hypothesis;
    }
}
