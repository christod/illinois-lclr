package edu.illinois.cs.cogcomp.esrl;

import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.comma.CommaLabeler;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.esrl.data.DAGViewGenerator;
import edu.illinois.cs.cogcomp.esrl.data.TEPair;
import edu.illinois.cs.cogcomp.esrl.latent.*;
import edu.illinois.cs.cogcomp.esrl.latent.features.Feature;
import edu.illinois.cs.cogcomp.esrl.latent.features.TEFeatureGenerator;
import edu.illinois.cs.cogcomp.esrl.latent.views.ISemanticDAG;
import junit.framework.TestCase;

import java.util.*;

public class ToyExampleTest extends TestCase {
    private TEFeatureGenerator fea;
    private WeightVector wv;
    private LexManager lm;
    private String parserView;
    private TEPair pair;

    public void setUp() throws Exception {
        super.setUp();
        Properties.readProperties("config/latent.properties");
        fea = new TEFeatureGenerator();
        lm = new LexManager();
        wv = new WeightVector(1.0);
        parserView = ViewNames.PARSE_STANFORD;

        // Preprocess the texts
        TextAnnotation taText = createText();
        TextAnnotation taHypothesis = createHypothesis();
        taText.addView(new DAGViewGenerator());
        taHypothesis.addView(new DAGViewGenerator());

        pair = new TEPair("test1", taText, taHypothesis, TEDataHandler.ENTAILMENT);
    }

    public void tearDown() {
        CachingCurator.closeCache();
    }

    public void testFeatureGenerator() {
        Map<String, Map<String, Double>> globalFeatureMap = new HashMap<>();
        ISemanticDAG textDAG = (ISemanticDAG) pair.getText().getView(DAGViewGenerator.VIEW_NAME);
        ISemanticDAG hypDAG = (ISemanticDAG) pair.getHypothesis().getView(DAGViewGenerator.VIEW_NAME);

        List<Constituent> textNodes = textDAG.getNodes();
        List<Constituent> hypNodes = hypDAG.getNodes();

        List<Relation> textRelations = textDAG.getEdges();
        List<Relation> hypRelations = hypDAG.getEdges();

        for (Constituent hypothesisConstituent : hypNodes) {
            if (hypothesisConstituent.getLabel().equals("TOP")) continue;

            for (Constituent textConstituent : textNodes) {
                if (textConstituent.getLabel().equals("TOP")) continue;

                Map<String, Double> feaMap = fea.getNodeAlignmentFeatures(textConstituent, hypothesisConstituent);
                globalFeatureMap.put(hypothesisConstituent + "\t" + textConstituent, feaMap);
            }
        }
        for (Relation hypothesisRelation : hypRelations) {
            if (hypothesisRelation.getSource().getLabel().equals("TOP")) continue;

            for (Relation textRelation : textRelations) {
                if (textRelation.getSource().getLabel().equals("TOP")) continue;

                Map<String, Double> feaMap = fea.getEdgeAlignmentFeatures(textRelation, hypothesisRelation);
                globalFeatureMap.put(hypothesisRelation + "\t" + textRelation, feaMap);
            }
        }
        Map<String, Double> map1 = globalFeatureMap.get("walking--SRL_VERB:A0--> man(1.0)\twith--SRL_PREP:Governor--> man(1.0)");
        Map<String, Double> map2 = globalFeatureMap.get("carrying--SRL_VERB:A1--> bag(1.0)\twith--SRL_PREP:Object--> bag(1.0)");
        assertEquals(0.5, map1.get("edge-different-views-args-match"));
        assertEquals(1.0, map2.get("edge-different-views-args-match"));
    }

    public void testILPAligner() {
        ILPAligner aligner = new ILPAligner(fea, lm, wv);
        Feature[] features = aligner.getBestFeatures(pair, "", true);
        assertEquals(1.2, features[0].getValue());
    }

    public void testLearning() {
        ILPAligner aligner = new ILPAligner(fea, lm, wv);
        List<TEPair> trainData = new ArrayList<>(1);
        trainData.add(pair);
        List<Integer> trainLabels = new ArrayList<>();
        trainLabels.add(1);
        wv.updateWeightVectorWithFullCDLatentSVM(aligner, trainLabels, trainData, 1, 5.0);
        double[] testScore = wv.predictScoreByFindingFeatures(aligner, "", trainData);
        assertEquals(1.0, testScore[0], 0.02);
    }

    private TextAnnotation createText() {
        TextAnnotation ta = new TextAnnotation("", "", Collections.singletonList("A man with a bag is walking ."));
        TokenLabelView lemmaView = createTokenLabelView(ta, "a man with a bag be walk .", ViewNames.LEMMA);
        ta.addView(ViewNames.LEMMA, lemmaView);
        TokenLabelView posView = createTokenLabelView(ta, "DT NN IN DT NN VBZ VBG .", ViewNames.POS);
        ta.addView(ViewNames.POS, posView);

        TreeView treeView = new TreeView(parserView, "test", ta, 1.0);
        treeView.setParseTree(0, Tree.readTreeFromString("(S1 (S (NP (NP (DT A) (NN man)) (PP (IN with) (NP (DT a) (NN bag)))) (VP (VBZ is) (VP (VBG walking))) (. .)))"));
        ta.addView(parserView, treeView);

        PredicateArgumentView verbSRLView = new PredicateArgumentView(ViewNames.SRL_VERB,"test", ta, 1.0);
        Constituent predicate = new Constituent("walking", ViewNames.SRL_VERB, ta, 6, 7);
        predicate.addAttribute(CoNLLColumnFormatReader.LemmaIdentifier, "walk");
        predicate.addAttribute(CoNLLColumnFormatReader.SenseIdentifer, "01");
        Constituent arg0 = new Constituent("", ViewNames.SRL_VERB, ta, 0, 5);
        verbSRLView.addConstituent(predicate);
        verbSRLView.addConstituent(arg0);
        verbSRLView.addRelation(new Relation("A0", predicate, arg0, 1.0));
        ta.addView(ViewNames.SRL_VERB, verbSRLView);

        ta.addView(ViewNames.SRL_NOM, new PredicateArgumentView(ViewNames.SRL_NOM, "test", ta, 1.0));

        PredicateArgumentView prepSRLView = new PredicateArgumentView(ViewNames.SRL_PREP, "test", ta, 1.0);
        predicate = new Constituent("with", ViewNames.SRL_PREP, ta, 2, 3);
        predicate.addAttribute(CoNLLColumnFormatReader.LemmaIdentifier, "Attribute");
        Constituent argGov = new Constituent("", ViewNames.SRL_PREP, ta, 0, 2);
        Constituent argObj = new Constituent("", ViewNames.SRL_PREP, ta, 3, 5);
        prepSRLView.addConstituent(predicate);
        prepSRLView.addConstituent(argGov);
        prepSRLView.addConstituent(argObj);
        prepSRLView.addRelation(new Relation("Governor", predicate, argGov, 1.0));
        prepSRLView.addRelation(new Relation("Object", predicate, argObj, 1.0));
        ta.addView(ViewNames.SRL_PREP, prepSRLView);

        ta.addView(ViewNames.NER, new SpanLabelView(ViewNames.NER, "test", ta, 1.0));

        ta.addView(ViewNames.COREF, new CoreferenceView(ViewNames.COREF, "test", ta, 1.0));

        ta.addView(CommaLabeler.VIEW_NAME, new PredicateArgumentView(CommaLabeler.VIEW_NAME, "test", ta, 1.0));
        return ta;
    }

    private TextAnnotation createHypothesis() {
        TextAnnotation ta = new TextAnnotation("", "", Collections.singletonList("The man carrying a bag is walking ."));
        TokenLabelView lemmaView = createTokenLabelView(ta, "the man carry a bag be walk .", ViewNames.LEMMA);
        ta.addView(ViewNames.LEMMA, lemmaView);

        TokenLabelView posView = createTokenLabelView(ta, "DT NN VBG DT NN VBZ VBG .", ViewNames.POS);
        ta.addView(ViewNames.POS, posView);

        TreeView treeView = new TreeView(parserView, "test", ta, 1.0);
        treeView.setParseTree(0, Tree.readTreeFromString("(S1 (S (NP (NP (DT The) (NN man)) (VP (VBG carrying) (NP (DT a) (NN bag)))) (VP (VBZ is) (VP (VBG walking))) (. .)))"));
        ta.addView(parserView, treeView);

        PredicateArgumentView verbSRLView = new PredicateArgumentView(ViewNames.SRL_VERB,"test", ta, 1.0);
        Constituent predicate = new Constituent("walking", ViewNames.SRL_VERB, ta, 6, 7);
        predicate.addAttribute(CoNLLColumnFormatReader.LemmaIdentifier, "walk");
        predicate.addAttribute(CoNLLColumnFormatReader.SenseIdentifer, "01");
        Constituent arg0 = new Constituent("", ViewNames.SRL_VERB, ta, 0, 5);
        verbSRLView.addConstituent(predicate);
        verbSRLView.addConstituent(arg0);
        verbSRLView.addRelation(new Relation("A0", predicate, arg0, 1.0));

        predicate = new Constituent("carrying", ViewNames.SRL_VERB, ta, 2, 3);
        predicate.addAttribute(CoNLLColumnFormatReader.LemmaIdentifier, "carry");
        predicate.addAttribute(CoNLLColumnFormatReader.SenseIdentifer, "01");
        arg0 = new Constituent("", ViewNames.SRL_VERB, ta, 0, 2);
        Constituent arg1 = new Constituent("", ViewNames.SRL_VERB, ta, 3, 5);
        verbSRLView.addConstituent(predicate);
        verbSRLView.addConstituent(arg0);
        verbSRLView.addConstituent(arg1);
        verbSRLView.addRelation(new Relation("A0", predicate, arg0, 1.0));
        verbSRLView.addRelation(new Relation("A1", predicate, arg1, 1.0));
        ta.addView(ViewNames.SRL_VERB, verbSRLView);

        ta.addView(ViewNames.SRL_NOM, new PredicateArgumentView(ViewNames.SRL_NOM, "test", ta, 1.0));

        ta.addView(ViewNames.SRL_PREP, new PredicateArgumentView(ViewNames.SRL_PREP, "test", ta, 1.0));

        ta.addView(ViewNames.NER, new SpanLabelView(ViewNames.NER, "test", ta, 1.0));

        ta.addView(ViewNames.COREF, new CoreferenceView(ViewNames.COREF, "test", ta, 1.0));

        ta.addView(CommaLabeler.VIEW_NAME, new PredicateArgumentView(CommaLabeler.VIEW_NAME, "test", ta, 1.0));
        return ta;
    }

    private TokenLabelView createTokenLabelView(TextAnnotation ta, String labelStr, String viewName) {
        TokenLabelView view = new TokenLabelView(viewName, "test", ta, 1.0);
        String[] labels = labelStr.split(" ");
        for (int i = 0; i < labels.length; i++) {
            view.addTokenLabel(i, labels[i], 1.0);
        }
        return view;
    }
}
