package edu.illinois.cs.cogcomp.esrl;

import edu.illinois.cs.cogcomp.cachingcurator.CachingCurator;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import junit.framework.TestCase;

public class AnnotatorTest extends TestCase {
    private Annotator annotator;

    public void setUp() throws Exception {
        super.setUp();
        annotator = Annotator.getInstance();
    }

    public void tearDown() {
        CachingCurator.closeCache();
    }

    public void testPreProcessText() throws Exception {
        String text = "John Smith wrote this sentence.";
        TextAnnotation ta = annotator.preProcessText(text);
        assertEquals(true, ta.hasView(ViewNames.SRL_VERB));
        assertEquals(true, ta.hasView(ViewNames.NER));
    }

    public void testGetLLM() throws Exception {
        String string1 = "this sentence";
        String string2 = "that sentence";
        double score = annotator.getLLM(string1, string2);
        assertEquals(1.0, score, 0.3);
    }

    public void testGetNESim() throws Exception {
        String entity1 = "Barack Obama";
        String entity2 = "Obama";
        double scoreStr = annotator.getNESim(entity1, entity2);
        assertEquals(1.0, scoreStr, 0.3);
    }

    public void testGetWNSim() throws Exception {
        String string1 = "arrested";
        String string2 = "involved";
        double score = annotator.getWNSim(string1, string2);
        assertEquals(0.027, score, 0.001);
    }
}